Format: 3.0 (quilt)
Source: intellij-idea-community
Binary: intellij-idea-community
Architecture: all
Version: 2016.2.4-1
Maintainer: Marcel Michael Kapfer <marcelmichaelkapfer@yahoo.co.nz>
Standards-Version: 3.9.6
Build-Depends: debhelper (>= 7.0.50~)
Package-List:
 intellij-idea-community deb devel optional arch=all
Checksums-Sha1:
 24860fcf28517431be055b7b8f20b0dea8b08f84 5637 intellij-idea-community_2016.2.4.orig.tar.gz
 2aa0ab52b5b6408a6fdf919055d9bdac0f1a4cf7 5672 intellij-idea-community_2016.2.4-1.debian.tar.xz
Checksums-Sha256:
 41958f809fdb7ce6640143c04c88233d51ab86e66be5c0a9e30e846af332550b 5637 intellij-idea-community_2016.2.4.orig.tar.gz
 1deec3a6da450b1d50d8105ca22ab01e0867cc3b66123ac686cc5a1d22d5c462 5672 intellij-idea-community_2016.2.4-1.debian.tar.xz
Files:
 e105950dd78a2260639bd06b848e4f3d 5637 intellij-idea-community_2016.2.4.orig.tar.gz
 1a367809594eb72b8672e83eb4171f73 5672 intellij-idea-community_2016.2.4-1.debian.tar.xz
